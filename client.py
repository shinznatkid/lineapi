# -*- coding: utf-8 -*-
"""
    line.client
    ~~~~~~~~~~~

    LineClient for sending and receiving message from LINE server.

    :copyright: (c) 2014 by Taehoon Kim.
    :license: BSD, see LICENSE for more details.
"""
import re
import sys
import time
import datetime

from .api import LineAPI
from .models import LineGroup, LineContact, LineRoom, LineMessage
from .curve.ttypes import TalkException, ToType, OperationType, Provider, Message
from .exceptions import LoginExpiredException, AnotherLoginException, AddContactUserExistsException, ContactNotFoundException, FindContactBlockedException

import sys
# If Python 2 Then set System Encoding to UTF-8
if sys.version[0] == '2':
    reload(sys)
    sys.setdefaultencoding("utf-8")

EMAIL_REGEX = re.compile(r"[^@]+@[^@]+\.[^@]+")


def auth_required(f):
    def wrapper(self, *args):
        if self.authToken:
            try:
                return f(self, *args)
            except TalkException as e:
                # code=1, 'Authentication Failed.'
                if e.code == 8 or e.code == 1:
                    result = self.login()
                    if result['success']:
                        return f(self, *args)
                    else:
                        raise LoginExpiredException('Login Failed.')
                raise
        else:
            raise LoginExpiredException('Authenticated required.')
    return wrapper


class LineClient(LineAPI):
    profile  = None
    contacts = []
    rooms    = []
    groups   = []

    def __init__(self, id=None, password=None, authToken=None, is_mac=True, com_name='WINDOWS-PC', revision=None):
        """Provide a way to communicate with LINE server.

        :param id: `NAVER id` or `LINE email`
        :param password: LINE account password
        :param authToken: LINE session key
        :param is_mac: (optional) os setting
        :param com_name: (optional) name of your system

        >>> client = LineClient("carpedm20", "xxxxxxxxxx")
        Enter PinCode '9779' to your mobile phone in 2 minutes
        >>> client = LineClient("carpedm20@gmail.com", "xxxxxxxxxx")
        Enter PinCode '7390' to your mobile phone in 2 minutes
        """
        super(LineClient, self).__init__()

        if not (authToken or id and password):
            msg = "id and password or authToken is needed"
            self.raise_error(msg)

        if is_mac:
            os_version = "10.9.4-MAVERICKS-x64"
            user_agent = "DESKTOP:MAC:%s(%s)" % (os_version, self.version)
            app = "DESKTOPMAC\t%s\tMAC\t%s" % (self.version, os_version)
        else:
            os_version = "5.1.2600-XP-x64"
            user_agent = "DESKTOP:WIN:%s(%s)" % (os_version, self.version)
            app = "DESKTOPWIN\t%s\tWINDOWS\t%s" % (self.version, os_version)

        if com_name:
            self.com_name = com_name

        self._headers['User-Agent']         = user_agent
        self._headers['X-Line-Application'] = app

        auth_pass = False
        if authToken:
            self.authToken = self._headers['X-Line-Access'] = authToken

            self.setProtocol()
            self.ready()
            time.sleep(1)
            try:
                self.getProfile()
                auth_pass = True
            except TalkException as e:
                if e.code == 8:  # AUTHENTICATION_DIVESTED_BY_OTHER_DEVICE
                    pass  # auth_pass = False
                elif e.code == 1:  # Authentication Failed.
                    pass  # รอ login ใหม่ข้างล่าง
                else:
                    raise
        if not auth_pass:
            if EMAIL_REGEX.match(id):
                self.provider = Provider.LINE  # LINE
            else:
                self.provider = Provider.NAVER_KR  # NAVER

            self.id = id
            self.password = password
            self.is_mac = is_mac

            self.login()
            self.ready()
            time.sleep(1)
            self.getProfile()

        time.sleep(1)
        self._login_revision = self._getLastOpRevision()
        if not revision:
            self.revision = self._login_revision
        elif self._login_revision > revision + 50:  # Get only latest 50 operations
            self.revision = self._login_revision - 50
        else:
            self.revision = revision
        # time.sleep(1)
        # self.refreshGroups()
        # time.sleep(1)
        # self.refreshContacts()
        # time.sleep(1)
        # self.refreshActiveRooms()
        # time.sleep(1)

    def getProfile(self):
        """Get `profile` of LINE account"""
        if self._check_auth():
            self.profile = LineContact(self, self._getProfile())

            return self.profile

        return None

    def updateStatusMessage(self, msg):
        """Get `profile` of LINE account"""
        if self._check_auth():
            return self._updateStatusMessage(msg)

        return False

    def getContactByName(self, name):
        """Get a `contact` by name

        :param name: name of a contact
        """
        for contact in self.contacts:
            if name == contact.name:
                return contact

        return None

    def getContactById(self, id):
        """Get a `contact` by id

        :param id: id of a contact
        """
        for contact in self.contacts:
            if contact.id == id:
                return contact

        return None

    def getContactOrRoomOrGroupById(self, id, update_if_not_found=False):
        """Get a `contact` or `room` or `group` by its id
        :param id: id of a instance
        """
        result = self.getContactById(id) or self.getRoomById(id) or self.getGroupById(id)
        if not result and update_if_not_found:
            if id.startswith('u'):
                contact = self.refreshContact(id)
                return contact
            else:
                time.sleep(1)
                self.refreshGroups()
                time.sleep(1)
                self.refreshContacts()  # Remove in next patch (cuz use refreshContact instead)
                time.sleep(1)
                self.refreshActiveRooms()
                try:
                    return self.getContactOrRoomOrGroupById(id)
                except:
                    return None
        return result

    def getBlockedContacts(self):
        if self._check_auth():
            contact_ids = self._getBlockedContactIds()
            contacts    = self._getContacts(contact_ids)
            contacts    = [LineContact(self, contact) for contact in contacts]
            contacts.sort()
            return contacts

    def refreshGroups(self):
        """Refresh groups of LineClient"""
        if self._check_auth():
            self.groups = []

            self.addGroupsWithIds(self._getGroupIdsJoined())
            time.sleep(1)
            self.addGroupsWithIds(self._getGroupIdsInvited(), False)

    def addGroupsWithIds(self, group_ids, is_joined=True):
        """Refresh groups of LineClient"""
        if self._check_auth():
            new_groups  = self._getGroups(group_ids)

            for group in new_groups:
                self.groups.append(LineGroup(self, group, is_joined))

            self.groups.sort()

    def refreshContacts(self):
        """Refresh contacts of LineClient """
        def chunk(iterable, n):
            from itertools import islice
            iterable = iter(iterable)
            while True:
                yield list(islice(iterable, n)) or iterable.next()

        if self._check_auth():

            contacts = []
            start_time = datetime.datetime.now()
            contact_ids = self._getAllContactIds()
            # print 'DEBUG: get ids time %s' % (datetime.datetime.now() - start_time)
            # TODO: try dynamic update only
            if contact_ids:
                for contact_ids_partition in chunk(contact_ids, 800):
                    contacts += self._getContacts(contact_ids_partition)
                    sys.stdout.write('.')
            # print 'DEBUG: len %s' % len(contact_ids)
            # start_time = datetime.datetime.now()
            # contacts    = self._getContacts(contact_ids)
            print('DEBUG: get contacts time %s' % (datetime.datetime.now() - start_time))

            self.contacts = []

            for contact in contacts:
                self.contacts.append(LineContact(self, contact))

            self.contacts.sort()

    @auth_required
    def refreshContact(self, mid):
        start_time = datetime.datetime.now()
        contacts = self._getContacts([mid])
        if not contacts:
            return  # Not found
        contact = contacts[0]
        contact = LineContact(self, contact)

        for i in range(len(self.contacts)):  # For loop in old way for replace self.contacts
            contact_id = self.contacts[i].id  # Comparer
            if contact_id == contact.id:
                self.contacts[i] = contact  # Replace
                print('DEBUG: get contact time %s' % (datetime.datetime.now() - start_time))
                return contact
        # Append
        self.contacts.append(contact)
        print('DEBUG: get contact time %s' % (datetime.datetime.now() - start_time))
        return contact

    def findAndAddContactByUserid(self, userid):
        """Find and add a `contact` by userid
        :param userid: user id
        """
        if self._check_auth():
            try:
                contact = self._findAndAddContactsByUserid(userid)
            except TalkException as e:
                self.raise_error(e.reason)

            contact = contact.values()[0]

            for c in self.contacts:
                if c.id == contact.mid:
                    raise AddContactUserExistsException("%s already exists" % contact.displayName)

            c = LineContact(self, contact)
            self.contacts.append(c)

            self.contacts.sort()
            return c

    def _findAndAddContactByPhone(self, phone):
        """Find and add a `contact` by phone number
        :param phone: phone number (unknown format)
        """
        if self._check_auth():
            try:
                contact = self._findAndAddContactsByPhone(phone)
            except TalkException as e:
                self.raise_error(e.reason)

            contact = contact.values()[0]

            for c in self.contacts:
                if c.id == contact.mid:
                    self.raise_error("%s already exists" % contact.displayName)
                    return

            c = LineContact(self, contact.values()[0])
            self.contacts.append(c)

            self.contacts.sort()
            return c

    def _findAndAddContactByEmail(self, email):
        """Find and add a `contact` by email
        :param email: email
        """
        if self._check_auth():
            try:
                contact = self._findAndAddContactsByEmail(email)
            except TalkException as e:
                self.raise_error(e.reason)

            contact = contact.values()[0]

            for c in self.contacts:
                if c.id == contact.mid:
                    self.raise_error("%s already exists" % contact.displayName)
                    return

            c = LineContact(self, contact.values()[0])
            self.contacts.append(c)

            self.contacts.sort()
            return c

    def findAndAddContactsByMid(self, mid):
        """
        Find a `contact` by mid
        """
        if self._check_auth():
            try:
                contacts = self._findAndAddContactsByMid(mid)
            except TalkException as e:
                if e.reason.startswith('Cannot find:'):
                    raise ContactNotFoundException(e.reason)
                else:
                    self.raise_error(e.reason)
            result = None
            for key, contact in contacts.items():
                result = LineContact(self, contact)
            return result

    def findContactByUserid(self, userid):
        """Find a `contact` by userid
        :param userid: user id
        """
        if self._check_auth():
            try:
                contact = self._findContactByUserid(userid)
            except TalkException as e:
                if(
                    e.reason.startswith('Cannot find:') or
                    e.reason == 'Invalid userid'
                ):
                    raise ContactNotFoundException(e.reason)
                elif e.reason == 'Blocked':
                    raise FindContactBlockedException(e.reason)
                else:
                    self.raise_error(e.reason)

            return LineContact(self, contact)

    def findContactByPhone(self, phones):
        """Find a `contact` by phones
        :param phones: phones
        """
        if self._check_auth():
            try:
                contact = self._findContactsByPhone(phones)
            except TalkException as e:
                if e.reason.startswith('Cannot find:'):
                    raise ContactNotFoundException(e.reason)
                else:
                    self.raise_error(e.reason)

            return LineContact(self, contact)

    def refreshActiveRooms(self):
        """Refresh active chat rooms"""
        # print('Rooms now disabled cuz LINE: Internal Error.')
        return
        if self._check_auth():
            start = 1
            count = 50

            self.rooms = []

            while True:
                time.sleep(0.5)
                channel = self._getMessageBoxCompactWrapUpList(start, count)
                if channel.messageBoxWrapUpList:
                    for box in channel.messageBoxWrapUpList:
                        if box.messageBox.midType == ToType.ROOM:
                            time.sleep(0.5)
                            room = LineRoom(self, self._getRoom(box.messageBox.id))
                            self.rooms.append(room)

                if channel.messageBoxWrapUpList is not None and len(channel.messageBoxWrapUpList) == count:
                    start += count
                else:
                    break

    def createGroupWithIds(self, name, ids=[]):
        """Create a group with contact ids

        :param name: name of group
        :param ids: list of contact ids
        """
        if self._check_auth():
            try:
                group = LineGroup(self, self._createGroup(name, ids))
                self.groups.append(group)

                return group
            except Exception as e:
                self.raise_error(e)

                return None

    def createGroupWithContacts(self, name, contacts=[]):
        """Create a group with contacts

        :param name: name of group
        :param contacts: list of contacts
        """
        if self._check_auth():
            try:
                contact_ids = []
                for contact in contacts:
                    contact_ids.append(contact.id)

                group = LineGroup(self, self._createGroup(name, contact_ids))
                self.groups.append(group)

                return group
            except Exception as e:
                self.raise_error(e)

                return None

    def getGroupByName(self, name):
        """Get a group by name

        :param name: name of a group
        """
        for group in self.groups:
            if name == group.name:
                return group

        return None

    def getGroupById(self, id):
        """Get a group by id

        :param id: id of a group
        """
        for group in self.groups:
            if group.id == id:
                return group

        return None

    def inviteIntoGroup(self, group, contacts=[]):
        """Invite contacts into group

        :param group: LineGroup instance
        :param contacts: LineContact instances to invite
        """
        if self._check_auth():
            contact_ids = [contact.id for contact in contacts]
            self._inviteIntoGroup(group.id, contact_ids)

    def acceptGroupInvitation(self, group):
        """Accept a group invitation

        :param group: LineGroup instance
        """
        if self._check_auth():
            try:
                self._acceptGroupInvitation(group.id)
                return True
            except Exception as e:
                self.raise_error(e)
                return False

    def leaveGroup(self, group):
        """Leave a group

        :param group: LineGroup instance to leave
        """
        if self._check_auth():
            try:
                self._leaveGroup(group.id)
                self.groups.remove(group)

                return True
            except Exception as e:
                self.raise_error(e)

                return False

    def createRoomWithIds(self, ids=[]):
        """Create a chat room with contact ids"""
        if self._check_auth():
            try:
                room = LineRoom(self, self._createRoom(ids))
                self.rooms.append(room)

                return room
            except Exception as e:
                self.raise_error(e)

                return None

    def createRoomWithContacts(self, contacts=[]):
        """Create a chat room with contacts"""
        if self._check_auth():
            try:
                contact_ids = []
                for contact in contacts:
                    contact_ids.append(contact.id)

                room = LineRoom(self, self._createRoom(contact_ids))
                self.rooms.append(room)

                return room
            except Exception as e:
                self.raise_error(e)

                return None

    def getRoomById(self, id):
        """Get a room by id

        :param id: id of a room
        """
        for room in self.rooms:
            if room.id == id:
                return room

        return None

    def inviteIntoRoom(self, room, contacts=[]):
        """Invite contacts into room

        :param room: LineRoom instance
        :param contacts: LineContact instances to invite
        """
        if self._check_auth():
            contact_ids = [contact.id for contact in contacts]
            self._inviteIntoRoom(room.id, contact_ids)

    def leaveRoom(self, room):
        """Leave a room

        :param room: LineRoom instance to leave
        """
        if self._check_auth():
            try:
                self._leaveRoom(room.id)
                self.rooms.remove(room)

                return True
            except Exception as e:
                self.raise_error(e)

                return False

    def sendMessage(self, message, seq=0):
        if not message:  # don't send empty message
            return
        """Send a message

        :param message: LineMessage instance to send
        """
        if self._check_auth():
            return self._sendMessage(message, seq)

    def getMessageBox(self, id):
        """Get MessageBox by id

        :param id: `contact` id or `group` id or `room` id
        """
        if self._check_auth():
            try:
                messageBoxWrapUp = self._getMessageBoxCompactWrapUp(id)

                return messageBoxWrapUp.messageBox
            except:
                return None

    def getRecentMessages(self, messageBox, count):
        """Get recent message from MessageBox

        :param messageBox: MessageBox object
        """
        if self._check_auth():
            id = messageBox.id
            messages = self._getRecentMessages(id, count)

            return self.getLineMessageFromMessage(messages)

    def longPoll(self, count=50):
        """Receive a list of operations that have to be processed by original
        Line cleint.
        :param count: number of operations to get from
        :returns: a generator which returns operations
        >>> for op in client.longPoll():
                sender   = op[0]
                receiver = op[1]
                message  = op[2]
                print "%s->%s : %s" % (sender, receiver, message)
        """
        if self._check_auth():
            """Check is there any operations from LINE server"""
            OT = OperationType
            try:
                operations = self._fetchOperations(self.revision, count)
            except EOFError:
                return
            except TalkException as e:
                print(e)
                if e.code == 9 or e.code == 8:
                    raise AnotherLoginException("user logged in to another machien")
                else:
                    return

            for operation in operations:
                if operation.type == OT.SEND_MESSAGE:
                    message = LineMessage(self, operation.message)
                    raw_sender = operation.message._from
                    sender = None
                    if self.profile.id == raw_sender:  # Me
                        sender = self.profile
                        # yield (sender, message)
                    print("[*] %s (%s -> %s)" % (OT._VALUES_TO_NAMES[operation.type], sender, message.receiver))
                    yield (operation.type, message)
                    pass
                elif operation.type == OT.RECEIVE_MESSAGE:
                    message    = LineMessage(self, operation.message)
                    raw_sender = operation.message._from
                    if self.profile.id == raw_sender:
                        sender = self.profile
                    else:
                        sender = message.sender  # use from exist model to optimized
                    if sender is None:
                        if message.receiver and isinstance(message.receiver, LineContact):
                            if self.revision >= self._login_revision:
                                self.refreshContact(raw_sender)
                                # self.refreshGroups()
                                # self.refreshContacts()
                                # self.refreshActiveRooms()
                            sender = self.getContactOrRoomOrGroupById(raw_sender)
                    print("[*] %s (%s -> %s)" % (OT._VALUES_TO_NAMES[operation.type], sender, message.receiver))
                    yield (operation.type, message)
                elif operation.type == OT.UPDATE_CONTACT:
                    contact = operation.param1
                    print("[*] %s (%s)" % (OT._VALUES_TO_NAMES[operation.type], contact))
                    yield (operation.type, operation)
                elif (
                    operation.type == OT.DUMMY or
                    operation.type == OT.NOTIFIED_RECOMMEND_CONTACT or
                    operation.type == OT.NOTIFIED_UPDATE_PROFILE or
                    operation.type == OT.SEND_CHAT_CHECKED or
                    operation.type == OT.NOTIFIED_INVITE_INTO_GROUP or
                    operation.type == OT.ACCEPT_GROUP_INVITATION or
                    operation.type == OT.NOTIFIED_UPDATE_PURCHASES or
                    operation.type == OT.NOTIFIED_UPDATE_GROUP or
                    operation.type == OT.UPDATE_PROFILE or
                    operation.type == OT.UPDATE_SETTINGS or
                    operation.type == OT.END_OF_OPERATION or
                    operation.type == OT.CANCEL_CALL or
                    operation.type == OT.SEND_CONTENT or
                    operation.type == OT.NOTIFIED_UPDATE_CONTENT_PREVIEW
                ):
                    print("[*] %s" % OT._VALUES_TO_NAMES[operation.type])
                elif (
                    operation.type == OT.ADD_CONTACT or
                    operation.type == OT.NOTIFIED_ADD_CONTACT or
                    operation.type == OT.NOTIFIED_REGISTER_USER or
                    operation.type == OT.NOTIFIED_UNREGISTER_USER or
                    operation.type == OT.SEND_CHAT_REMOVED or
                    operation.type == OT.BLOCK_CONTACT or
                    operation.type == OT.UNBLOCK_CONTACT
                ):
                    if self.revision >= self._login_revision:
                        # self.refreshContacts()
                        self.refreshContact(operation.param1)
                    contact = self.getContactById(operation.param1)
                    print("[*] %s %s" % (OT._VALUES_TO_NAMES[operation.type], contact))
                elif operation.type == OT.CREATE_ROOM:
                    if self.revision >= self._login_revision:
                        self.refreshActiveRooms()
                    room = self.getRoomById(operation.param1)
                    print("[*] %s %s" % (OT._VALUES_TO_NAMES[operation.type], room))
                elif (
                    operation.type == OT.CREATE_GROUP or
                    operation.type == OT.UPDATE_GROUP
                ):
                    if self.revision >= self._login_revision:
                        self.refreshGroups()
                    room = self.getGroupById(operation.param1)
                    print("[*] %s %s" % (OT._VALUES_TO_NAMES[operation.type], room))
                elif (
                    operation.type == OT.NOTIFIED_INVITE_INTO_ROOM or
                    operation.type == OT.NOTIFIED_ACCEPT_GROUP_INVITATION or
                    operation.type == OT.UNKNOWN_OPERTAION_TYPE60 or
                    operation.type == OT.NOTIFIED_LEAVE_GROUP or
                    operation.type == OT.UNKNOWN_OPERTAION_TYPE61 or
                    operation.type == OT.INVITE_INTO_ROOM or
                    operation.type == OT.NOTIFIED_LEAVE_ROOM or
                    operation.type == OT.INVITE_INTO_GROUP
                ):
                    rooms = [self.getContactOrRoomOrGroupById(x) for x in operation.param1.split('\x1e')]
                    if len(rooms) != 1:
                        room = ', '.join(unicode(rooms))
                    else:
                        room = rooms[0]
                    invited = self.getContactById(operation.param2)
                    print("[*] %s (%s -> %s)" % (OT._VALUES_TO_NAMES[operation.type], invited, room))
                elif operation.type == OT.NOTIFIED_READ_MESSAGE:
                    chat = self.getContactOrRoomOrGroupById(operation.param1)
                    reader = self.getContactOrRoomOrGroupById(operation.param2)  # Maybe only Contact
                    # param3 = chat_room.id
                    print("[*] %s (%s -> %s)" % (OT._VALUES_TO_NAMES[operation.type], reader, chat))
                    yield (operation.type, operation)
                elif operation.type == OT.RECEIVE_MESSAGE_RECEIPT:
                    reader = self.getContactOrRoomOrGroupById(operation.param1)
                    print("[*] %s (%s)" % (OT._VALUES_TO_NAMES[operation.type], reader))
                    yield (operation.type, operation)
                else:
                    print("[*] %s" % OT._VALUES_TO_NAMES[operation.type])
                    print(operation)

                self.revision = max(operation.revision, self.revision)

    def createContactOrRoomOrGroupByMessage(self, message):
        if message.toType == ToType.USER:
            pass
        elif message.toType == ToType.ROOM:
            pass
        elif message.toType == ToType.GROUP:
            pass

    def getLineMessageFromMessage(self, messages=[]):
        """Change Message objects to LineMessage objects

        :param messges: list of Message object
        """
        lineMessages = []

        for message in messages:
            lineMessages.append(LineMessage(self, message))

        return lineMessages

    def _check_auth(self):
        """Check if client is logged in or not"""
        if self.authToken:
            return True
        else:
            msg = "you need to login"
            self.raise_error(msg)

    def _send_message(self, lineid, text):
        message = Message(lineid, text)
        return self.sendMessage(message)

    @auth_required
    def updateContactSetting(self, mid, setting_flag):
        return self._updateContactSetting(mid, setting_flag)
